# Onyx
<snippet>
![Alt](http://nsa38.casimages.com/img/2016/05/22/160522121516524814.png)

Onyx a pour but de vous simplifier la vie , son but premier est de tout réunir en un seul endroit

Vous voulez consulter vos mails , ajouter des rendez-vous , avoir la météo et régler vos réveils en même temps ? C'est le but d'Onyx , de tout centraliser.

Imaginons :

Vous rentrez chez vous ,vous lancez votre ordinateur et votre page d'accueil est Onyx , puis vous vous connectez avec votre compte personnel et vous tombez sur votre page d'accueil rempli de vos widgets que vous avez choisis au préalable

Vous décidez de lancer votre musique préférée pendant que vous consultez vos mails sur la mailbox.

Puis vous allez programmer votre réveil pour le lendemain avec votre musique préférée pour vous réveiller

Le lendemain Onyx vous réveille avec votre musique puis vous indique l'heure et la météo du jour pour vous permettre de vous habiller de la bonne façon , de plus , il vous donne vos rendez-vous de la journée qui vienne directement de votre compte Google !

**Le site :** [http://onyxlabs.fr](http://onyxlabs.fr)

## Installation

Pour installer Onyx vous pouvez le faire directement depuis pip avec la commande :

pip install onyxproject


et lancer Onyx avec la commande :

sudo onyxstart

## Liens 

- [Le site](http://onyxlabs.fr)
- [Le blog](http://blog.onyxlabs.fr)
- [Le trello](https://trello.com/b/1HObWTNb/onyx-development)
- [Le twitter](https://twitter.com/LabsOnyx)


</snippet>
